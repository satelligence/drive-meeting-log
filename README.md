# Meetings Log
A log like personal Google Drive viewer with Markdown parsing and search.

http://satelligence.gitlab.io/drive-meeting-log/

After you authorize it to read your Google Drive files, it will search for files
starting with the default `KEYWORDS = ['meeting', 'note', 'notulen']`. It
displays all the files within those folders and all the files with filenames
starting with one of the keywords. Start typing to search in the files.

It currently parses Google documents and text files as markdown, all other files
only get a link to Drive. Meeting notes get a nice layout by using
[`[markdown](www.markdown.com)`](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

### Fork
If you want something like this, but different: fork this, but get your own
`CLIENT_ID`. This ID will only work from satelligence.gitlab.io. At time of
writing you can do this [here](https://console.developers.google.com/apis/credentials).

Easiest adaptation is to make this log work with other `KEYWORDS`. All other
behavior is somewhere in the 250 lines in `script.js` or in the Google Drive
API.
