// Client ID and API key from the Developer Console
var CLIENT_ID = '412046396273-dbe1h5t6i6oape1ggimt5iev1ah1tlj4.apps.googleusercontent.com';

// Keywords to look for on drive
var KEYWORDS = ['meeting', 'note', 'notulen'];

// Array of API discovery doc URLs for APIs
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];

// Authorization scopes required by the API
var SCOPES = 'https://www.googleapis.com/auth/drive.readonly';

var LOADING_FOLDERS = 'Getting folders...';
var LOADING_FILES = 'Loading files...';
var LONG_TIME = 2500; // ms
var STILL_LOADING = 'Google is very busy today...';
var AUTHORIZE = 'Not authorized.';
var NO_DATA = 'No meetings. None. All for nothing... :(';

// Timer starts when doing
var timer;
var initTimer;

// UI elements
var authorizeButton = document.getElementById('authorize-button');
var signoutButton = document.getElementById('signout-button');
var moreButton = document.getElementById('more');
var cursor = document.getElementById('cursor');
var cmd = document.getElementById('cmd');
var query = document.getElementById('query');
var input = document.getElementById('input');
var content = document.getElementById('content');
var empty = document.getElementById('empty');
var emptyMessage = document.getElementById('empty-message');

// All folders to include in file searches
var folders;

// Request for file list. Cancelled for consecutive calls.
var requestFiles = {};

// Requests for file content
var fileRequests = [];

// Blinking cursor interval
var blink;

// Search clause
var searchClause = "name contains '" +
  KEYWORDS.join("' or name contains '") +
  "'";

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    getFolders().then(getFiles);

    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';

    input.disabled = false;
    input.oninput = function () {
      query.textContent = input.value;
      content.innerHTML = '';
      getFiles(folders, input.value.length ? input.value : null)
    };

  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
    input.disabled = true;
    moreButton.style.display = 'none';
  }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();
  content.innerHTML = '';
  updateSigninStatus(false);
}

function addPlaceholderRow(metadata) {

  var row = document.createElement("DIV");
  row.id = metadata.id;
  row.classList.add('row');

  var leftColumn = document.createElement("DIV");
  leftColumn.classList.add('col-md-3');
  leftColumn.innerHTML = '<blockquote class="blockquote blockquote-reverse">' +
    '<footer class="blockquote-footer">' +
      '<em>' + new Date(metadata.modifiedTime).toDateString() + '</em>' +
    '</footer>' +
  '</blockquote>'

  var rightColumn = document.createElement("DIV");
  rightColumn.classList.add('col-md-8');
  rightColumn.classList.add('align-self-center');
  var html = '<p>';
  if (metadata.lastModifyingUser.photoLink) {
    html += '<span><img class="rounded-circle mlog-avater" src="' +
      metadata.lastModifyingUser.photoLink +
      '" height="40"></span>';
  } else {
    html += '<span>' + metadata.lastModifyingUser.displayName + ' </span';
  }
  html += '<span><a href="'+ metadata.webViewLink + '">' + metadata.name + '</a></span></p>';
  rightColumn.innerHTML = html;

  row.append(leftColumn);
  row.append(rightColumn);
  content.appendChild(row);
}

function addContentToContentRow(id, driveContent) {
  var el = document.getElementById(id);

  if (!el) {
    // Drive content is no longer relevant;
    return;
  }

  var row = document.createElement("DIV");
  row.classList.add('row');

  var leftColumn = document.createElement("DIV");
  leftColumn.classList.add('col-md-3');

  var rightColumn = document.createElement("DIV");
  rightColumn.classList.add('col-md-8');

  rightColumn.innerHTML = window.marked(driveContent);

  row.append(leftColumn);
  row.append(rightColumn);

  content.insertBefore(row, el.nextSibling);
};

function catchAPIError (e) {
  if (e.name === 'cancel') {
    clearInterval(timer);
    setCommandValue(null);
  } else {
    setCommandValue('Do you have a problem with me?');
  }
};

function getFolders() {

  if (query.innerHTML === AUTHORIZE) {
    setCommandValue(LOADING_FOLDERS);
    initTimer = setTimeout(function () {
      setCommandValue(STILL_LOADING);
    }, LONG_TIME);
  }

  // Get all the folders containing meetings or notes or notulen.
  return gapi.client.drive.files.list({
    pageSize: 1000,
    fields: "nextPageToken, files(id)",
    corpus: 'DOMAIN',
    spaces: 'drive',
    q: "mimeType = 'application/vnd.google-apps.folder' and (" + searchClause + ")"
  })

  .then(function (folderResponse) {
    clearTimeout(initTimer);
    focusInput();
    if (query.innerHTML === LOADING_FOLDERS || query.innerHTML === STILL_LOADING) {
      setCommandValue(LOADING_FILES);
    }
    folders = folderResponse.result.files;
    return folders;
  })
};

/**
 * Print files.
 */
function getFiles(folders, fullText, pageToken) {

  // Cancel all requests.
  if (requestFiles.cancel) { requestFiles.cancel(); }
  fileRequests.forEach(function(r) { r.cancel(); });
  fileRequests = [];

  clearInterval(timer);
  var counter = 0;
  timer = setInterval(function () {
    if (counter++ < 2) {
      setCommandValue(LOADING_FILES);
    } else if (counter > 1) {
      setCommandValue(STILL_LOADING);
    }
  }, LONG_TIME);
  empty.style.display = 'none';

  // Get all the files in the folders and all the files with names resembling notes or meetings.
  var ids = folders.map(function(folder) { return folder.id; });
  var q = "mimeType != 'application/vnd.google-apps.folder' and (" + searchClause;
  if (ids.length) {
     q += " or '" +
     ids.join("' in parents or '") +
     "' in parents";
  }
  q += ')';
  if (fullText) {
    q += " and fullText contains '" + fullText + "'";
  }
  requestFiles = gapi.client.drive.files.list({
    pageSize: 10,
    orderBy: 'modifiedTime desc',
    fields: "nextPageToken, files(id, name, mimeType, webViewLink, modifiedTime, lastModifyingUser)",
    corpus: 'DOMAIN',
    spaces: 'drive',
    q: q,
    pageToken: pageToken
  })

  .then(function (filesResponse) {
    var nextPageToken = filesResponse.result.nextPageToken;
    if (nextPageToken) {
      moreButton.style.display = 'inline';
      moreButton.innerHTML = 'Show more';
      moreButton.onclick = function () {
        moreButton.innerHTML = 'Loading...';
        getFiles(folders, fullText, nextPageToken)
      }
    } else {
      moreButton.style.display = 'none';
    }

    return filesResponse;
  }, catchAPIError)

  .then(function (filesResponse) {

    if (filesResponse.result.files.length === 0) {
      empty.style.display = 'block';
      emptyMessage.innerHTML = NO_DATA;
    } else {
      empty.style.display = 'none';
    }

    // Get all the files.
    filesResponse.result.files.forEach(function (file) {
      addPlaceholderRow(file);
      var request;
      if (file.mimeType === 'application/vnd.google-apps.document') {
        request = gapi.client.drive.files.export({
          fileId: file.id,
          mimeType: 'text/plain'
        });
      }
      else if (
        file.mimeType === 'text/plain' ||
        file.mimeType === 'text/markdown'
      ) {
        request = gapi.client.drive.files.get({fileId: file.id, alt: 'media'})
      }

      if (request) {
        fileRequests.push(
          request.then(function (driveContent) {
            clearInterval(timer);
              setCommandValue(null);
            addContentToContentRow(file.id, driveContent.body);
          }, catchAPIError)
        );
      }

    });

    if (fileRequests.length === 0) {
      clearInterval(timer);
      setCommandValue(null);
    }
  });

}

function setCommandValue (message) {
  if (message) {
    query.textContent = message;
  } else {
    query.textContent = input.value;
  }
};

setCommandValue(AUTHORIZE);

focusInput = function () {
  input.focus()
  clearInterval(blink);
  var counter = 0;
  cursor.style.display = 'none';
  blink = window.setInterval(function() {
    if (cursor.style.display === 'block') {
      cursor.style.display = 'none';
    } else {
      cursor.style.display = 'block';
    }
    if (counter++ > 8) {
      cursor.style.display = 'block';
      clearInterval(blink);
    }
  }, 400);
};

cmd.onclick = function () {
  query.textContent = input.value;
  focusInput()
}
